# OFEP0: OpenFlexure Enhancement Proposals

| OFEP                               | 0                                  |
|------------------------------------|------------------------------------|
| Author                             | Julian Stirling                    |
| Created                            | 14-Nov-2020                        |
| Status                             | Approved                           |
| Approved date                      | 2-Dec-2020                         |
| Requires implementation            | No                                 |
| Implemented date                   | N/A                                |
| Updated dates (post-approval)      | N/A                                |

Following the model of Python where important changes are first proposed as Python Enhancement Proposals (PEPs). This is designed to provide a concise technical description of enhancements and the rationale for the feature.

## 1. Why OFEPs?

As we move forward from an purely academic project to a project that manufacturers can build into a product we need to layout design rationale more clearly. This does not replace good commit messages, this does not replace issue and merge request discussions, these are all important parts of the project history. The OFEPs, however, will provide more detailed clarity on important changes and will remain as technical documentation for those who wish to understand a specific aspect of the project.

## 2. Process

1. New OFEPs should be proposed (by anyone) as merge requests using the template in the templates folder. Copy this into a new folder named OFEP9999 and leave the number as 9999 in the document until merge is approved. A merge can be opened partially formed to allow others to participate in it's formation.
1. Discussion will happen in this merge request thread.
1. The core development team will meet to decide whether to approve the changes. Richard Bowman will have a veto on any OFEPs approvals as project maintainer. If there is significant interest in the OFEP from those external to the core team they will be invited to this meeting. During the meeting the OFEP will either be:
    * **Approved**
    * **Rejected**
    * **Delayed pending changes**.  
1. If approved, any final minor edits will be applied to the text.
1. The OFEP number will then be assigned and the Status will be changed to **Approved**, and the approval date set.
1. If the OFEP requires specific code changes (some may be simply informative, or guidance for future direction). The status will be changed to **Approved, not implemented**
1. Once implemented in the relevant repository a new merge request can be opened. This should set the implemented date and change the status to **Approved and implemented**
1. This merge request will be discussed on GitLab before merging. Unless there is significant disagreement on the implementation this should not need a meeting for approval.
