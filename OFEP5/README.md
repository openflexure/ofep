
# OFEP5: Standardising the electronics of the microscope

| OFEP                               | 5                               |
|------------------------------------|------------------------------------|
| Author                             | Richard Bowman                     |
| Created                            | 01-May-2024                        |
| Status                             | Approved, not implemented          |
| Approved date                      | 08-May-2024                        |
| Requires implementation            | Yes                                |
| Implemented date                   | N/A                                |
| Updated dates (post-approval)      | 31-Jul-2024                        |

## 1. Introduction

The motor control electronics for the microscope are probably the most ambiguous part. It would be great to have one clear option that gives best results, with provision for work-arounds to make sure the project stays accessible to people who can't source the preferred option. The Sangaboard v0.5 is now clearly the neatest option, and I propose we should adopt both that board, and the associated illumination board, as the default electronics for the microscope. The solution based on discrete boards (Arduino Nano/Pi Pico plus three unipolar Darlington pair drivers) should remain as a supported option for builders who can't get hold of the Sangaboard easily.

## 2. Current state

The instructions are currently a bit vague and confusing about electronics: many options are mentioned, including a work-around using a microcontroller board and at least three different versions of the Sangaboard. This is unhelpful to builders, who have wasted time and effort trying to source boards that aren't produced commercially, or surface mount soldering with limited success. 

Similarly, multiple options exist for the illumination. The LED work-around is properly documented, but the board used in the instructions was never produced other than for our lab, and is different to the boards now sold with the Sangaboard v5.

### 2.1. Sangaboard v0.5

In the last couple of years, the Sangaboard v0.5 has been developed (by Filip Ayazi, together with Valerian Sanga) and is now sold via [Taulab].  Filip has also designed a breakout board for the LED, controlled by the Sangaboard v0.5's on-board constant current driver. Over 100 units have been produced and used, in our lab and elsewhere, and the board solves a number of longstanding issues:

* Power handling is improved, and now uses USB-C to permit higher currents. This has reduced or eliminated reliability issues due to undervoltage.
* The HAT form factor eliminates inter-board wiring in the microscope, resulting in a smaller and significantly neater product.
* Serial communication with the Raspberry Pi frees up a USB port and removes the need for unsightly cabling.
* A single USB port removes the ambiguity of having one port for communications and another port for power.
* The RP2040 microcontroller avoided chip shortages of the ATMega chips we used previously.
* A programmable constant-current driver allows control of the LED.
* Provision for other hardware (a fourth motor axis and two MOSFET channels, as well as additional breakout pins) will enable future expandability.
* Firmware uploads no longer require the Arduino IDE.

Perhaps the most significant step is that this board is now able to be purchased via [Taulab]: a lack of commercial availability was the principal barrier to most builders using Sangaboards in the past.

The Sangaboard and its firmware are openly developed on GitLab, currently in Filip's personal namespace. These repositories don't all have documentation and licenses, and this would be needed in some form to officially adopt the project. Similarly, working firmware exists and has been tested, but the built binary file is not archived in a prominent place and the tooling required to build it (`platformio`) is not something most microscope builders are likely to set up without significant guidance and effort.

## 3. Proposed change

We propose to adopt Sangaboard v0.5 and it's associated LED breakout board as the default electronics option, retaining support for the discrete board solution to avoid reliance on custom boards available from a single supplier. This will require the microscope documentation to be updated, and will also require us to make sure the Sangaboard project is properly documented, licensed, and archived. Items that would need action to implement this OFEP are marked with tick boxes [ ] below. Many of these are, or will be, implemented by related merge request [!363](https://gitlab.com/openflexure/openflexure-microscope/-/merge_requests/363).

### 3.1. Changes to sangaboard project

We would need to ensure clear documentation and license information exists for the Sangaboard and its firmware. This may be as simple as ensuring a good README exists in each repository, along with an OSI-approved `License` file (ideally a `CERN-OHL-?` version). There are currently five relevant repositories:

* https://gitlab.com/filipayazi/sangaboard-rp2040 contains the hardware designs: KiCAD source files plus manufacturable Gerber files.
  * [ ] This repository has no README file. We should make a merge request to add one.
  * [x] This repository has no License, and this should be added too.
  * [ ] *Optional:* It would be great to add this to Kitspace - and I think the Gerber and BOM files may make this relatively simple.
* https://gitlab.com/filipayazi/sangaboard-firmware contains the associated firmware, built using the `platformio` framework.
  * [ ] This repository has a README file, but it should be updated to mention Sangaboard v5.
  * [x] This repository has no License, and this should be added.
  * [ ] *Optional* We should add CI to build the binary firmware for Arduino Nano and for RP2040-based boards, so that the binaries are available on a build server (a Zenodo archive would be an acceptable interim solution).
  * [ ] We should add, or link to, clear instructions on how to upload the firmware.
* https://gitlab.com/filipayazi/sangaboard-extension adds features to the microscope server, such as aborting moves and uploading firmware
  * This will break with the release of software `v3`, but aborting moves is now built-in and much more elegant.
  * The key functionality this provides is automatic firmware updates. This should be moved into the management interface in due course, and/or provided as a command-line tool on the microscope's SD card image.
  * There is a README but no License - a License would be good, but we'd not be relying on this repository in any case.
* https://gitlab.com/bath_open_instrumentation_group/pysangaboard contains the Python code to communicate with the Sangaboard
  * A License and README are provided
  * Microscope builders needn't be aware of this library - it's used by the sangaboard class in the microscope server, and is available on pypi.
  * Various forks of the repo exist, but none of them have anything Sangaboard v5-specific. The default version supports v5, though not all features are exposed.
* https://gitlab.com/filipayazi/ofm-led provides the illumination board, with editable KiCAD source files and machine-readable BOM.
  * A README is provided
  * [x] A License is needed.
  * [ ] Add Gerber files to the repository.
  * [ ] *Optional:* add this project to Kitspace (requires Gerber files).
  * [ ] We should clarify with Filip what LED is used in these boards. If it's not the same as the one used on the constant current boards, we should document that, ideally alongside some tests.
* [ ] We should ensure a permanent archive of source and build products exists on e.g. Zenodo, to help with reproducibility. Specifically, we should create a Zenodo archive of the above five repositories, together with binaries of the firmware for RP2040 and Arduino.

### 3.2. Deprecated projects

We would also need to ensure that a number of other repositories are clearly marked as being old versions both in the repo and on Kitspace, specifically:

* [ ] https://gitlab.com/openflexure/openflexure-constant-current-illumination (GitLab + Kitspace)
* [ ] https://gitlab.com/openflexure/openflexure-simple-illumination (GitLab + Kitspace)
* [ ] https://gitlab.com/bath_open_instrumentation_group/sangaboard (GitLab + Kitspace)

### 3.3. Changes to instructions

We should:

* [ ] Replace the rendered LED board and Sangaboard with v5, showing the correct colour and shape of the boards.
* [ ] Update the sangaboard part page.
* [ ] Update the LED board part page.
* [ ] Update the electronics instructions text.
* [ ] Update the image showing the LED connector.
* [ ] Add a work-around page describing the older constant-current board.
* [ ] Add clear links to instructions describing the non-default options.
* [ ] Add visual steps confirming that the electronics you are using are the correct hardware version.
* [ ] Review instructions around uploading firmware.

The electronics instructions could use some polish, including rendered images. That is separate from the changes required to implement this OFEP.

## 4. Alternatives and rejected options

Keeping the 5v constant current board was the recommendation at a project call in [June 2023](https://gitlab.com/openflexure/design-review/-/blob/master/Minutes/2023_06_13_project_call.md) but this was before Sangaboard v5 and the associated illumination board were available from [Taulab]. At present, the only way to obtain the constant current board is to build or order the assembled PCBs yourself from e.g. JLC, so it does not make sense as the default option. While some of those boards do still exist "in the wild" and should be documented as an alternative, it is not likely to be used by many people.

[Taulab]: https://taulab.eu/

## 5. Implementation

* License files were added to the repositories (reviewed 31 July 2024)