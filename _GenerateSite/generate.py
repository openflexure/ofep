#! /usr/bin/env python3

'''
This creates a base website for OFEPs.
This is not another static site generator. `mkdocs` and
`jekyll` require a specific format which make writing OFEPs annoying.
'''

import os
import shutil
import re
from markdown import markdown
from jinja2 import Environment, FileSystemLoader

def _pop_starting_with(pathlist, beginning):
    '''
    Useful little function to speed up directory walking. From GitBuilding
    '''
    poplist = []
    for i, path in enumerate(pathlist):
        if path.startswith(beginning):
            poplist.append(i)
    #Pop them from the directory list in reverse order to keep indexes valid
    for i in poplist[::-1]:
        pathlist.pop(i)

def _get_title(md):
    headings = re.findall(r"^#(?!#)[ \t]*(.*)$",
                          md,
                          re.MULTILINE)
    if len(headings) == 0:
        raise RuntimeError('OFEP does not have title')
    return headings[0]

class WebsiteMaker():
    '''
    This class makes a website. Oh dear, maybe I am making a static site
    generator :(
    '''
    def __init__(self):
        loader = FileSystemLoader(['_GenerateSite'])
        self.env = Environment(loader=loader, trim_blocks=True)
        self._ofeps = []

    @property
    def ofeps(self):
        return sorted(self._ofeps, key=lambda ofep: ofep[0])

    def copy_static(self):
        '''
        Copies in some static files taken from openflexure.org
        '''
        static_dir = '_GenerateSite/static/'
        for static_file in os.listdir(static_dir):
            filepath = os.path.join(static_dir, static_file)
            outpath = os.path.join('_site', static_file)
            shutil.copyfile(filepath, outpath)

    def get_html(self, md, root="./"):
        '''
        Simple makdown to html converter
        '''

        # Convert GitLab flavoured markdown amth syntax into a format KaTeX will auto render
        # Note we do this rather than modify the autorender as otherwise the markdown converter will
        # catch the back-ticked tex code
        md = re.sub(r'\$`([^`]+)`\$', r'$\g<1>$', md)
        md = re.sub(r'```math((?:.|\n)*?)```', r'$$\g<1>$$', md)

        content_html = markdown(md, extensions=["markdown.extensions.tables",
                                                "markdown.extensions.attr_list",
                                                "markdown.extensions.fenced_code",
                                                "markdown_checklist.extension"])
        tmpl = self.env.get_template("template.jinja")
        html = tmpl.render(title="OpenFlexure Enhancement Proposals",
                           content=content_html,
                           root=root)

        return html

    def make_ofep(self, number, filename):
        '''
        Creates the main page for an OFEP
        '''
        with open(filename, 'r') as file_obj:
            md = file_obj.read()
        title = _get_title(md)
        html = self.get_html(md, root="../")
        directory = os.path.join("_site", f"OFEP{number}")
        if not os.path.exists(directory):
            os.mkdir(directory)
        outfile = os.path.join(directory, 'index.html')
        with open(outfile, 'w') as file_obj:
            file_obj.write(html)
        self._ofeps.append((number, title))

    def make_ofep_index(self):
        """
        Make an index to select OFEPs
        """
        md = "# OpenFlexure Enhancement Proposals\n\n"
        md += "To find out more about OpenFlexure Enhancement Proposals (OFEPs) "
        md += "check OFEP0 below. Alternatively check the [OFEP repository on "
        md += "GitLab](https://gitlab.com/openflexure/ofep).\n\n"
        md += "## List of approved OFEPs:\n\n"
        for ofep_number, title in self.ofeps:
            md += f"* [{title}](./OFEP{ofep_number})\n"
        html = self.get_html(md, root="./")
        index_page = os.path.join("_site", "index.html")
        with open(index_page, 'w') as file_obj:
            file_obj.write(html)

def main():
    '''Quick script to copy the relevant files'''

    if os.path.exists('_site'):
        raise RuntimeError('Cannot overwtite "_site". Please delete.')
    os.mkdir('_site')

    if not os.path.exists('README.md'):
        raise RuntimeError('Are you executing this from the OFEP main_directory?')

    web_maker = WebsiteMaker()
    web_maker.copy_static()

    web_maker.make_ofep(0, 'README.md')

    for root, dirs, files in os.walk('.'):
        # From GitBuilding, This just ignores everything in hidden
        # Underscored directories
        _pop_starting_with(dirs, '.')
        _pop_starting_with(files, '.')
        _pop_starting_with(dirs, '_')
        _pop_starting_with(files, '_')

        ofep_match = re.match(r'^\.(?:\/|\\)OFEP([0-9]+)$', root)
        if ofep_match:
            ofep_no = ofep_match.group(1)
            ofep_file = os.path.join(root, 'README.md')
            if not os.path.exists(ofep_file):
                raise RuntimeError("Found OFEP without README.md")
            web_maker.make_ofep(int(ofep_no), ofep_file)
            for directory in dirs:
                dir_now = os.path.join(root, directory)
                dir_location = os.path.join("_site", "OFEP"+ofep_no, directory)
                shutil.copytree(dir_now, dir_location)
            for fname in files:
                if fname != 'README.md':
                    file_now = os.path.join(root, fname)
                    file_location = os.path.join("_site", "OFEP"+ofep_no, fname)
                    shutil.copytree(file_now, file_location)
    web_maker.make_ofep_index()

if __name__ == "__main__":
    main()
